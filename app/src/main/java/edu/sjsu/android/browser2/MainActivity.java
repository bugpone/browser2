package edu.sjsu.android.browser2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = findViewById(R.id.editText);

        // Get the intent that started this activity
        Intent intent = getIntent();

        Uri data = intent.getData();
        display.setText("moo");
        display.setText(data.toString());
        Log.d("MDEBUG",data.toString());



    }
}
